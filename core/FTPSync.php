<?php

namespace ftpsync;

class FTPSync {
    private $changes;
    private $dirind;
    private $remote_dir;
    private $maxtry;

    public function __construct($changes, $dir, $remote_dir, $maxtry) {
        $this->changes = $changes;
        $this->dirind = strlen($dir) + 1;
        $this->remote_dir = $remote_dir;
        $this->maxtry = $maxtry;
    }

    private function get_ftp_path($path) {
        return substr($path, $this->dirind);
    }

    private function delete($stream, $path) {
        $path = $this->get_ftp_path($path);
        if (@ftp_size($stream, $path) === -1) return;
        for ($i = 0; $i < $this->maxtry; $i++) {
            if (!@ftp_delete($stream, $path)) {
                error_log($path . ' error delete');
                continue;
            }
            break;
        }
        $dir = dirname($path);
        if ($dir === '.') return;
        if (count(ftp_nlist($stream, $dir)) === 0) {
            if (!ftp_rmdir($stream, $dir)) {
                error_log($dir . ' error delete');
            }
        }
    }

    private function mkdirs($stream, $path){
        @ftp_chdir($stream, $this->remote_dir);
        foreach(explode('/', $path) as $part){
            if (empty($part)) continue;
            if (!@ftp_chdir($stream, $part)) {
                for ($i = 0; $i < $this->maxtry; $i++) {
                    if (!@ftp_mkdir($stream, $part)) {
                        error_log($path . ' error mkdir in ' . $part . ' part');
                        continue;
                    }
                    break;
                }
                @ftp_chdir($stream, $part);
            }
        }
    }

    private function upload($stream, $path) {
        $ftp_path = $this->get_ftp_path($path);
        $dir = dirname($ftp_path);
        if ($dir != '.') {
            $this->mkdirs($stream, $dir);
            @ftp_chdir($stream, $this->remote_dir);
        }
        @ftp_chdir($stream, $dir);
        for ($i = 0; $i < $this->maxtry; $i++) {
            if (!@ftp_put($stream, basename($ftp_path), $path, FTP_BINARY)) {
                error_log($path . ' error upload');
                continue;
            }
            break;
        }
        @ftp_chdir($stream, $this->remote_dir);
    }

    /**
    * login to FTP server
    * @return stream or null
    */
    public function login($ssl, $host, $port, $timeout, $user, $password) {
        for ($i = 0; $i < $this->maxtry; $i++) {
            try {
                $stream;
                if ($ssl) {
                    $stream = ftp_ssl_connect($host, $port, $timeout);
                } else {
                    $stream = ftp_connect($host, $port, $timeout);
                }
                if (!$stream) continue;
                if (ftp_login($stream, $user, $password)) return $stream;
            } catch(Exception $e) {
                error_log($e->__toString());
            }
        }
        return null;
    }

    /**
    * sync dirs and files
    * @param $stream
    */
    public function sync($stream) {
        ftp_chdir($stream, $this->remote_dir);
        foreach($this->changes['del'] as $path) {
            $this->delete($stream, $path);
        }
        foreach(array_keys($this->changes['new']) as $path) {
            $this->upload($stream, $path);
        }
        foreach(array_keys($this->changes['change']) as $path) {
            $this->upload($stream, $path);
        }
    }

    public function close($stream) {
        ftp_close($stream);
    }
}
