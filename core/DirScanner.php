<?php

namespace ftpsync;

class DirScanner {
    private $dir;
    private $hash_algo;
    private $old_struct;
    private $exclude;

    public function __construct($dir, $hash_algo, $old_struct, $exclude) {
        $this->dir = $dir;
        $this->hash_algo = $hash_algo;
        $this->old_struct = $old_struct;
        $this->exclude = $exclude;
    }

    private function get_list($dir) {
        $result = [];
        foreach(scandir($dir) as $path) {
            if ($path === '.' || $path === '..') continue;
            $path = $dir . '/' . $path;
            if (in_array($path, $this->exclude)) continue;
            if (is_dir($path)) {
                $result = array_merge($result, $this->get_list($path));
            } else {
                $result[] = $path;
            }
        }
        return $result;
    }

    private function scan_new($list) {
        $result = [];
        foreach($list as $path) {
            if (array_key_exists($path, $this->old_struct)) continue;
            $result[$path] = hash_file($this->hash_algo, $path);
        }
        return $result;
    }

    private function scan_del($list) {
        $result = [];
        foreach(array_keys($this->old_struct) as $path) {
            if (in_array($path, $list)) continue;
            $result[] = $path;
        }
        return $result;
    }

    private function scan_change($list, $exclude) {
        $result = [];
        foreach($list as $path) {
            if (in_array($path, $exclude)) continue;
            $hash = hash_file($this->hash_algo, $path);
            if ($this->old_struct[$path] === $hash) continue;
            $result[$path] = $hash;
        }
        return $result;
    }

    /**
    * scan
    * @return array [new => [path => hash], del => [], change => [path => hash], list => []]
    */
    public function scan() {
        $list = $this->get_list($this->dir);
        $new_files = $this->scan_new($list);
        return [
            'new' => $new_files,
            'del' => $this->scan_del($list),
            'change' => $this->scan_change(
                $list, array_merge($this->exclude, array_keys($new_files))
            )
        ];
    }
}
