<?php

namespace ftpsync;

class JsonStorage {
    private $path;

    public function __construct($path) {
        $this->path = $path;
    }

    public function load() {
        if (!file_exists($this->path)) return [];
        return json_decode(file_get_contents($this->path), true);
    }

    private function merge($old_struct, $data) {
        $result = array_merge($old_struct, $data['new']);
        foreach($data['del'] as $path) {
            unset($result[$path]);
        }
        return $result;
    }

    public function save($old_struct, $data) {
        file_put_contents($this->path, json_encode($this->merge($old_struct, $data)));
    }
}
