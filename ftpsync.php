<?php

namespace ftpsync;

require_once(__DIR__ . '/core/DirScanner.php');
require_once(__DIR__ . '/core/FTPSync.php');
require_once(__DIR__ . '/core/JsonStorage.php');
require_once(__DIR__ . '/config.php');

echo 'scan changes...' . PHP_EOL;
$storage = new JsonStorage(__DIR__ . '/storage.json');
$old_struct = $storage->load();
$scanner = new DirScanner($dir, $hash_algo, $old_struct, $exclude);
$data = $scanner->scan();
if (empty($data)) {
    die('not changes, exit...' . PHP_EOL);
}
echo 'start FTP sync...' . PHP_EOL;
$ftp = new FTPSync($data, $dir, $ftp_dir, $ftp_maxtry);
$stream = $ftp->login($ftp_ssl, $ftp_host, $ftp_port, $ftp_timeout, $ftp_user, $ftp_password);
if ($stream === null) {
    die('FTP login failed' . PHP_EOL);
}
$ftp->sync($stream);
$ftp->close($stream);
echo 'save data...' . PHP_EOL;
$storage->save($old_struct, $data);
echo 'done' . PHP_EOL;
