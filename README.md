# FTPSync

Directory synchronize via FTP. Features:

* upload new files
* upload changed files (check hash)
* delete deleted files

## Run

```
php ftpsync.php
```

## Settings

Edit **config.php**

* **hash_algo** - hash algorithm: md5, sha256 and other
* **dir** - local directory for synchronize
* **exclude** - exclude full local paths
* **ftp_maxtry** - try limit for FTP operations
* **ftp_dir** - FTP sync directory path
* and other options
