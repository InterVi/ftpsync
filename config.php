<?php

$hash_algo = 'md5'; //hash algorithm: md5, sha256 and other
$dir = '/path/to/dir'; //local directory for synchronize
//exclude full local paths
$exclude = [
    'baddir',
    'baddir2'
];
$ftp_host = '127.0.0.1';
$ftp_port = 21;
$ftp_timeout = 30;
$ftp_user = 'user';
$ftp_password = 'password';
$ftp_ssl = false;
$ftp_maxtry = 3; //try limit for FTP operations
$ftp_dir = '/remote/path'; //FTP sync directory path
